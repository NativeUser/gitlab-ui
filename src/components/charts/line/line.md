### Line Chart

This is a basic line chart.

### Implementation Details

This component wraps the Gitlab UI `chart` component, which in turn wraps the ECharts component.

See [gitlab-ui/chart](./?path=/story/charts-chart--default) for more info.
